<?php 
/*
Plugin Name: Integnity (Formcraft to Mailchimp)
Plugin URI: http://integnity.com
Description: Sending data to Mailchimp on successful form submission
Version: 1.0
Author: Tom Balev @ Integnity
Author URI: http://integnity.com
License: GPL2
*/

/* ==========================

[TABLE OF CONTENTS]

1 - Helpers
2 - Hooks
2 - Functions
3 - MAIN




========================== */

// Prevent direct file access
defined( 'ABSPATH' ) or exit;

/* ==========================
   Helper: Write Log
========================== */
if (!function_exists('write_log')) {
    function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}

/* ==========================
   Helper: Verify Envato purchase code
========================== */
function integ_FCtoMC__verify_purchase_code_envato($license_code){
    require('envato_api.php');
    $envato = new EnvatoAPI('fgxoPnVOfxEgpwmS5pCYXiErm4oZKIKB');

    $envatoObj = $envato->call('/author/sale?code='.$license_code);

    return $envatoObj;
}


/* ==========================
   Filters
========================== */
// Make URL query variables available
function custom_query_vars_filter($vars) {
  $vars[] = 'useremail';
  $vars[] = 'licensecode';
  $vars[] = 'redirectform';
  $vars[] = 'timerDate';
  return $vars;
}
add_filter( 'query_vars', 'custom_query_vars_filter' );
 


/* ==========================
   Create db table structure on plugin activation

   - ID
   - form_id
   - form_name
   - submission_time
   - email
   - license
   - rating
========================== */
function integ_FCtoMC_create_table_structure() {
    write_log("CREATING TABLE STRUCTURE");

    global $wpdb;
    $table_name = $wpdb->prefix . "integnity_feedback_submissions";
    $charset_collate = $wpdb->get_charset_collate();

      $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      form_id INT NOT NULL,
      form_name tinytext NOT NULL,
      created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      submitted_to_marketplace INT NOT NULL,
      email tinytext NOT NULL,
      license tinytext NOT NULL,
      rating INT NOT NULL,
      UNIQUE KEY id (id)
      ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );   
}

register_activation_hook( __FILE__, 'integ_FCtoMC_create_table_structure' );




/* ==========================
   Change form fields on submit
========================== */
function my_function($entry_content){

    /* ==========================
       Active Feedback Forms
    ========================== */
    $feedbackActiveProducts = array(
        '13922688' => 'photomotion',
        '11582301' => 'ultimate_app_promo_motion'
        );
    /* ==========================
       [end] Active Feedback Forms
    ========================== */

    $redirectValue = '';

    foreach ($entry_content as &$item) {

        /* ==========================
           Verify Envato license + redirect
        =============================
        =============================
            1.  if there is field license_value_envato_verify
                it will be verified with Envato API
            2.  if Envato returned success and there is "redirect_value"
                get redirect URL query by ID and write it into redirect value field
        ========================== */

        if ($item["label"] == "license_value_envato_verify") {

            $license_code = $item["value"];
            $envatoObj = integ_FCtoMC__verify_purchase_code_envato($license_code);

            write_log("Verifying license code");

            // if Envato API call returned success
            if (!isset($envatoObj->error)){

                
                /* ==========================
                   // Redirection setup
                ========================== */
                // if there is redirect_value field, set it to envato value
                foreach ($entry_content as &$item){
                        if ($item["label"] == "redirect_value") {

                            // Photo Motion
                            if (array_key_exists($envatoObj->item->id, $feedbackActiveProducts)){

                                // Set redirect
                                $redirectValue = $feedbackActiveProducts[$envatoObj->item->id];
                                $item["value"] = $redirectValue;

                                write_log("Will be redirecting to: " . $redirectValue);

                            // No feedback form
                            } else {
                                write_log("(FC to MC): There is no feedback form for this product.");

                                // Show error message
                                global $fc_final_response;
                                $fc_final_response['failed'] = "There is no feedback form for this product.";
                                echo json_encode($fc_final_response);
                                die();                     
                            }
                        }
                    }

            // if Envato API call returned failure
            } else {
                write_log("(FC to MC): That doesn't look like a valid purchase code");

                // Show error message
                global $fc_final_response;
                $fc_final_response['failed'] = "That doesn't look like a valid purchase code";
                echo json_encode($fc_final_response);
                die(); 
            }
        }
        /* ==========================
           [end] Envato verify license + redirect
        ========================== */  


    }

    return $entry_content;
}
add_filter('formcraft_filter_entry_content', 'my_function');


/* ==========================
   MAIN: Send new rating to Mailchimp
========================== */
function integ_FCtoMC__get_data_from_FC($content, $meta, $raw_content, $integrations){

    // Prepare vars to be written to our internal table

    $formID = $content["Form ID"];
    $formName = $content["Form Name"];

    // Feedback form trigger
    // Used to trigger following functions, if not present, feedback form logic will be ignored
    if (strpos($formName, 'product_feedback') !== false ) {

        $formFeedbackFormTrigger = 1;

        // Email
        if(array_key_exists("email_value", $content)) {
            $formUserEmail = $content["email_value"];
        } else {
            $formUserEmail = '';
        }     
        
        // Rating
        if(array_key_exists("rating_value", $content)) {
            $formRating = $content["rating_value"];
        } else {
            $formRating = '';
        }       

        // License
        if(array_key_exists("license_value", $content)) {
            $formLicense = $content["license_value"];
        } else if (array_key_exists("license_value_envato_verify", $content)) {
            $formLicense = $content["license_value_envato_verify"];
        } else {
            $formLicense = '';
        }   

        // Submitted to marketplace
        if(array_key_exists("submitted_to_marketplace", $content)) {
            $formSubmittedToMarketplace = $content["submitted_to_marketplace"];

            if ($formSubmittedToMarketplace == "Yes") {
                $formSubmittedToMarketplace = 1;
            }

        } else {
            $formSubmittedToMarketplace = 0;
        }

    } else {
        $formFeedbackFormTrigger = 0;
    } 


    if ($formFeedbackFormTrigger !== 0 && $formID !== '' && $formName !== '') {

        global $wpdb;
        $table_name = $wpdb->prefix . 'integnity_feedback_submissions';  

        $formLicense_from_db = $wpdb->get_var( $wpdb->prepare( 
            "
                SELECT rating
                FROM $table_name
                WHERE license = %s
            ", 
            $formLicense
        ));         


        // Check if the license is in db already
        if (!$formLicense_from_db) {
            write_log("NO PREVIOUS SUBMISSIONS FOUND FOR THIS PURCHASE CODE. ADDING...");
            // add new entry to db
            $wpdb->insert( 
                $table_name, 
                array(
                    'form_id' => $formID,
                    'form_name' => $formName,
                    'submitted_to_marketplace' => $formSubmittedToMarketplace,
                    'created' => current_time( 'mysql' ), 
                    'email' => $formUserEmail, 
                    'license' => $formLicense,
                    'rating' => $formRating
                ) 
            );   

            // Send new data to Mailchimp
            integ_FCtoMC__MAIN_send_rating_to_MC($formRating, $formUserEmail, $formLicense, $formSubmittedToMarketplace);

        } else if($formLicense_from_db != 5) {
            // Feedback for this review submitted already
            // Check if there is a difference in rating
            write_log("PREVIOUS SUBMISSION FOUND, BUT RATING WASN'T EQUAL TO 5. COMPARE OLD AND NEW RATING, THEN UPDATE ACCORDINGLY...");

            if($formLicense != $formLicense_from_db) {

                $wpdb->update( 
                    $table_name, 
                    array( 
                        'rating' => $formRating,
                        'email' => $formUserEmail,
                        'submitted_to_marketplace' => $formSubmittedToMarketplace
                    ), 
                    array( 'license' => $formLicense ), 
                    array( 
                        '%d',
                        '%s',
                        '%d'
                    ), 
                    array( '%s' ) 
                );

                write_log($formUserEmail);

                // Send new data to Mailchimp
                integ_FCtoMC__MAIN_send_rating_to_MC($formRating, $formUserEmail, $formLicense, $formSubmittedToMarketplace);                 
            }
            
        } else {
            write_log("THIS LICENSE IS IN OUR DB ALREADY, AND RATINGS ARE EQUAL");
        }
        
    } else {
        write_log('THIS WAS NOT A FEEDBACK FORM, as it was missing product_feedback from its name, skipping further logic');
    }

}

// Call before saving form to database
add_action('formcraft_after_save', 'integ_FCtoMC__get_data_from_FC', 10, 4);


/* ==========================
   MAIN: Send new rating to Mailchimp
========================== */
function integ_FCtoMC__MAIN_send_rating_to_MC($rating,$email,$license_code, $submittedToMarketplace) {

    // Vars
    $apiKey = get_option('integ_MC__apiKey');
    $list_id = '0e53be12ea';

    // Get Mailchimp user object
    $MC_user_object = integ_FCtoMC__get_subscriber_by_email($apiKey,$list_id ,$email);

    // Write new rating to Mailchimp, if user found
    if ($MC_user_object->status !== 404){

        $active_license1 = $MC_user_object->merge_fields->LICCODE_1;
        $active_license2 = $MC_user_object->merge_fields->LICCODE_2;  

        // Send rating to Mailchimp
        if ($active_license1 == $license_code || $active_license2 == $license_code) {
            write_log("(FC to MC): License matches, write rating value to Mailchimp");
            integ_FCtoMC__write_rating($apiKey,$list_id ,$email, $rating, $submittedToMarketplace);
        } else {
            write_log("(FC to MC): License submitted from FC doesn't match what we have on record for this email address. Abort operation");
        }              

    } else {
        write_log("MAILCHIMP ERROR: email not found in Mailchimp, aborting mailchimp api call");
    }

}




/* ==========================
   Get subscribers object from Mailchimp
========================== */
function integ_FCtoMC__get_subscriber_by_email($apiKey,$list_id ,$email) {

    // Check mandatory fields
    if ($apiKey != '' && $email != '' && $list_id != '') {

        $data = [];

        $url = 'https://' . substr($apiKey,strpos($apiKey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5($email) ;
        $result = json_decode( integ_MC__api2( $url, 'GET', $apiKey, $data) );

        return $result;

    } else {
        write_log("(FC to MC): Email, List ID or API key not supplied");
    }
}


/* ==========================
   Write new rating to Mailchimp
========================== */
function integ_FCtoMC__write_rating($apiKey,$list_id ,$email, $rating, $submittedToMarketplace) {

        $merge_fields = array(
            'FRATING' => $rating,
            'FPOSTED' => $submittedToMarketplace
        );

        // Setup data
        $data = array('merge_fields' => $merge_fields);

        $url = 'https://' . substr($apiKey,strpos($apiKey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5($email) ;
        $result = json_decode( integ_MC__api2( $url, 'PATCH', $apiKey, $data) );



        // Send confirmation email to customer
        $data_confirmation = array('email_address' => $email);
        $url_confirmation = 'https://' . substr($apiKey,strpos($apiKey,'-')+1) . '.api.mailchimp.com/3.0/automations/17f2bdb570/emails/2db855ced9/queue';
        $result_confirmation = json_decode( integ_MC__api2( $url_confirmation, 'POST', $apiKey, $data_confirmation) );            

        write_log($result_confirmation);

}

/* ==========================
   Mailchimp API 3.0 CALL
========================== */
function integ_MC__api2( $url, $request_type, $api_key, $data = array() ) {
    if( $request_type == 'GET' )
        $url .= '?' . http_build_query($data);
 
    $mch = curl_init();
    $headers = array(
        'Content-Type: application/json',
        'Authorization: Basic '.base64_encode( 'user:'. $api_key )
    );
    curl_setopt($mch, CURLOPT_URL, $url );
    curl_setopt($mch, CURLOPT_HTTPHEADER, $headers);
    //curl_setopt($mch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch, CURLOPT_RETURNTRANSFER, true); // do not echo the result, write it into variable
    curl_setopt($mch, CURLOPT_CUSTOMREQUEST, $request_type); // according to MailChimp API: POST/GET/PATCH/PUT/DELETE
    curl_setopt($mch, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch, CURLOPT_SSL_VERIFYPEER, false); // certificate verification for TLS/SSL connection
 
    if( $request_type != 'GET' ) {
        curl_setopt($mch, CURLOPT_POST, true);
        curl_setopt($mch, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
    }
 
    return curl_exec($mch);
}


/* ==========================
   [End] Mailchimp code
========================== */



/* ==========================
   Admin menu
========================== */

// create custom plugin settings menu
add_action('admin_menu', 'int_fc_to_mc_create_menu');

function int_fc_to_mc_create_menu() {

    //create new top-level menu
    add_menu_page('Integnity FC to MC Settings', '(Int) FC to MC', 'administrator', __FILE__, 'int_fc_to_mc_settings_page' , plugins_url('/images/icon.png', __FILE__) );

    //call register settings function
    add_action( 'admin_init', 'register_int_fc_to_mc_settings' );
}


function register_int_fc_to_mc_settings() {
    //register our settings
    register_setting( 'int-fc-to-mc-settings-group', 'integ_MC__apiKey' );
}

function int_fc_to_mc_settings_page() {
?>


<div class="wrap">
<h1>Integnity - Formcraft to Mailchimp</h1>

<form method="post" action="options.php">
    <?php settings_fields( 'int-fc-to-mc-settings-group' ); ?>
    <?php do_settings_sections( 'int-fc-to-mc-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Mailchimp API</th>
        <td><input type="text" name="integ_MC__apiKey" value="<?php echo esc_attr( get_option('integ_MC__apiKey') ); ?>" /></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>

<?php }


// https://us15.api.mailchimp.com/3.0/automations/a3b126324c/emails/07424e5382/queue

?>